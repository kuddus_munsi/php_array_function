

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>php array function practise</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<section class="content">


    <header class="header">
        <h2>Php <u>array_diff_key</u> function practise</h2>
    </header>

    <div class="maincontent">

        <?php
        $array1 =
            array(
                "a" => "red",
                "b" => "orange",
                "c" => "blue",
                "d" => "white",
            );
        $array2= array(
            "a" => "red",
            "f" => "y",
            "c" => "yu",
            "d" => "green",
        );


        $difference = array_diff_key($array1 , $array2);

        echo "<pre>";
        print_r($difference);
        echo "</pre>";
        ?>


    </div>

    <footer class="footer">
        <h2>Hi!! welcome to array function practise</h2>
    </footer>
</section>

</body>
</html>