

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>php array function practise</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<section class="content">


    <header class="header">
        <h2>Php <u>array_map</u> function practise</h2>
    </header>

    <div class="maincontent">

        <?php
       /*function myfunction($value){

           return $value+$value;
       }
       $arr = array (1,2,3,4,5,6);

       $result= array_map("myfunction", $arr);*/

       function myfunction($value){
           $strVal = strtoupper($value);
           return $strVal;
       }
      $arr = array(
            "name" => "kuddus",
            "age" => "twentyfour",
            "batch" => "twentyfive"

        );
       $result = array_map("myfunction", $arr);
        echo "<pre>";
        print_r($result);
        echo "</pre>";
        ?>
    </div>

    <footer class="footer">
        <h2>Hi!! welcome to array function practise</h2>
    </footer>
</section>

</body>
</html>