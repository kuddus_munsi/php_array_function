

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>php array function practise</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<section class="content">


    <header class="header">
        <h2>Php <u>array_replace</u> function practise</h2>
    </header>

    <div class="maincontent">

        <?php
        $array1= array("a"=>"sathi","b"=>"munsi");
        $array2= array("a"=>"abdul","b"=>"kuddus");

        $reslt = array_replace($array1,$array2);

        echo "<pre>";
        print_r($reslt);
        echo "</pre>";

        ?>
    </div>

    <footer class="footer">
        <h2>Hi!! welcome to array function practise</h2>
    </footer>
</section>

</body>
</html>