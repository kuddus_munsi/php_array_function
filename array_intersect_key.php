

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>php array function practise</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<section class="content">


    <header class="header">
        <h2>Php <u>array_intersect_key</u> function practise</h2>
    </header>

    <div class="maincontent">

        <?php
        $array1 =
            array(
                "a" => "red",
                "b" => "orange",
                "c" => "blue",
                "s" => "white",
            );
        $array2= array(
            "a" => "www",
            "b" => "orange",
            "c" => "blue",
            "s" => "ddd",
        );


        $intersect = array_intersect_key($array1 , $array2);

        echo "<pre>";
        print_r($intersect);
        echo "</pre>";
        ?>


    </div>

    <footer class="footer">
        <h2>Hi!! welcome to array function practise</h2>
    </footer>
</section>

</body>
</html>