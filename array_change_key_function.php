

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>php array function practise</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<section class="content">


    <header class="header">
        <h2>Php "array_change_key_case"</h2>
    </header>

    <div class="maincontent">

<?php
$name = array(

    "Kuddus" =>25,
    "Mizan" => 26,
    "AMZAD" => 24,
    "Shuva" => 25,
    "Tonmoy" => 22,
    "Tonmoy" => 220
) ;

echo "<pre>";
print_r(array_change_key_case($name,CASE_LOWER))."<br>";
echo "</pre>";
?>


</div>

    <footer class="footer">
        <h2>Hi!! welcome to array function practise</h2>
    </footer>
</section>

</body>
</html>