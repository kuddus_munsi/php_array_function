

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>php array function practise</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<section class="content">


    <header class="header">
        <h2>Php <u>array_intersect_key</u> function practise</h2>
    </header>

    <div class="maincontent">

        <?php
        $array1 =
            array(
                "name" => "kuddus",
                "age" => "24"

            );

        if(array_key_exists("name" , $array1)){
            echo "the key is exist";
        }
        else
            echo "the key is not in";
        ?>


    </div>

    <footer class="footer">
        <h2>Hi!! welcome to array function practise</h2>
    </footer>
</section>

</body>
</html>