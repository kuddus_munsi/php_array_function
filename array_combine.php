

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>php array_combine function practise</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<section class="content">


    <header class="header">
        <h2>Php Array function practise</h2>
    </header>

    <div class="maincontent">

        <?php
        $name = array("kuddus", "JAS" ,"Munsi");
        $age= array(25,18,24);

        $combine = array_combine($name, $age);

        echo "<pre>";
        print_r($combine);
        echo "</pre>";
        ?>


    </div>

    <footer class="footer">
        <h2>Hi!! welcome to array function practise</h2>
    </footer>
</section>

</body>
</html>