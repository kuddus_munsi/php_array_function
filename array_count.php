

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>php array function practise</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<section class="content">


    <header class="header">
        <h2>Php array_count_values function practise</h2>
    </header>

    <div class="maincontent">

        <?php
        $name = array("kuddus", "JAS" ,"Munsi","kuddus", "JAS" ,"Munsi","kuddus", "JAS");
        $age= array(25,18,24,18,24,18,24);

        $combine = array_count_values($age);

        echo "<pre>";
        print_r($combine);
        echo "</pre>";
        ?>


    </div>

    <footer class="footer">
        <h2>Hi!! welcome to array function practise</h2>
    </footer>
</section>

</body>
</html>