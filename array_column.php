

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>php array function practise</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<section class="content">


    <header class="header">
        <h2>Php array_column function practise</h2>
    </header>

    <div class="maincontent">

        <?php
        $name = array(
            array (

                    "id" => 200,
                    "first_name" => "md",
                    "last_name" => "kuddus"
            ),
             array (

                 "id" => 201,
                 "first_name" => "kuddus",
                 "last_name" => "munsi"
             ),
                  array (

                      "id" => 202,
                      "first_name" => "amzad",
                      "last_name" => "hossain"
                  )
        ) ;
 $lastName = array_column($name , "last_name", "id");
        echo "<pre>";
        print_r($lastName);
        echo "</pre>";
        ?>


    </div>

    <footer class="footer">
        <h2>Hi!! welcome to array function practise</h2>
    </footer>
</section>

</body>
</html>